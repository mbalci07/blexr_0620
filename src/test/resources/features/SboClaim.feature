Feature: The bookie selector function

  @wipBonus
  Scenario: “CLAIM” button link should not be broken with the following options
    Given User should be on sbo.net site.
    When  User clicks on the Bookie Selector
    And   Select the following following choices Desktop, €50 - €200, Yes
    Then  Verify that CLAIM button link is not broken.


