package com.blexr.runners;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(

        plugin = {"json:target/cucumber.json",
                "html:target/defaut-html-reports",
        },
        features = "src/test/resources/features/",
        glue = "com/blexr/stepDefinitions/",
        dryRun = false,
        tags = "@wipBonus"
)

public class CukesRunner {


}
