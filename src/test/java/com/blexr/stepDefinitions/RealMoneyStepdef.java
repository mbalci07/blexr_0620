package com.blexr.stepDefinitions;

import com.blexr.pages.RealMoney;
import com.blexr.utilities.Driver;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebElement;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class RealMoneyStepdef {
    @Given("user should be on real money page")
    public void user_should_be_on_real_money_page() {
        Driver.get().get("https://www.vegasslotsonline.com/real-money/");
    }

    @Then("Verify that following links on real money page are matching")
    public void verify_that_following_links_on_real_money_page_are_matching(List<String> dataTable) throws InterruptedException {
        RealMoney realMoney=new RealMoney();
        int count =0;
        for (WebElement links: realMoney.clickLinks()) {
            links.click();
            Thread.sleep(5000);
            String actualURL = Driver.get().getCurrentUrl();
            String expUrl  = "https://www.vegasslotsonline.com"+dataTable.get(count);
            count++;
            assertEquals(expUrl,actualURL);
            realMoney.realMoneyTab.click();

        }
    }
}
