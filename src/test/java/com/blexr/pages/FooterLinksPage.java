package com.blexr.pages;


import com.blexr.utilities.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;
import java.util.List;

public class FooterLinksPage {
    public FooterLinksPage() {
        PageFactory.initElements(Driver.get(), this);
    }


    @FindBy(xpath = "//div[@class='col-md-9 bottom-menu']//ul//li/a")
    public List<WebElement> footerLinks;

    @FindBy(xpath = "//*[text()='Contact Us']")
    public WebElement ContactUs;

    @FindBy(xpath = "//*[text()='Privacy Policy']")
    public WebElement PrivacyPolicy;

    public void footerLinksClik(String linkName) {
        String Elxpath = "//div[@class='col-md-9 bottom-menu']/ul/li/a[text()='"+linkName+"']";
        WebElement linkElement = Driver.get().findElement(By.xpath(Elxpath));
        linkElement.click();
    }

    public List<String> footerLinksInfo(){
        List<String>footerLabelText= new ArrayList<>();
        for (WebElement label : footerLinks) {
            footerLabelText.add(label.getText());
        }
        return footerLabelText;
    }
}
