package com.blexr.stepDefinitions;

import com.blexr.pages.FooterLinksPage;
import com.blexr.utilities.ConfigurationReader;
import com.blexr.utilities.Driver;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.junit.Assert;
import org.openqa.selenium.WebElement;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.List;

public class footer_stepdef {
    @Given("User should be see home page")
    public void user_should_be_see_home_page() {
        Driver.get().get(ConfigurationReader.get("homePage"));
    }

    @Then("Verify each link is not broken")
    public void verify_each_link_is_not_broken() {
        String url = "";
        HttpURLConnection huc = null;
        int respCode = 200;
        FooterLinksPage footerLinksPage = new FooterLinksPage();
        List<WebElement> links = footerLinksPage.footerLinks;
        Iterator<WebElement> it = links.iterator();

        while (it.hasNext()) {

            url = it.next().getAttribute("href");


            try {
                huc = (HttpURLConnection) (new URL(url).openConnection());

                huc.setRequestMethod("HEAD");

                huc.connect();

                respCode = huc.getResponseCode();

                Assert.assertEquals(200, respCode);

                if (respCode >= 400) {
                    System.out.println(url + " is a broken link");
                } else {
                    System.out.println(url + " is a valid link");
                }

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

    }
}
