package com.blexr.stepDefinitions;

import com.blexr.pages.SboClaim;
import com.blexr.utilities.Driver;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class SboClaimStepDefs {
    SboClaim sboClaim = new SboClaim();


    @Given("User should be on sbo.net site.")
    public void user_should_be_on_sbo_net_site() {
        Driver.get().get("https://www.sbo.net/alternatives/");
        Driver.get().manage().window().maximize();

    }

    @When("User clicks on the Bookie Selector")
    public void user_click_on_the_Bookie_Selector() {
        sboClaim.BookieSelector.click();

    }

    @When("Select the following following choices Desktop, €{int} - €{int}, Yes")
    public void select_the_following_following_choices_Desktop_€_€_Yes(Integer int1, Integer int2) throws InterruptedException {

        sboClaim.desktop.click();
        sboClaim.money50200.click();
        sboClaim.yes.click();

        while (!sboClaim.claim.isDisplayed()) {

            sboClaim.startagain.click();
            sboClaim.desktop.click();
            sboClaim.money50200.click();
            sboClaim.yes.click();
            Thread.sleep(3000);
        }
    }

    @Then("Verify that CLAIM button link is not broken.")
    public void verify_that_CLAIM_button_link_is_not_broken() {
        SboClaim sboClaim = new SboClaim();
        String url = "";
        HttpURLConnection huc = null;
        int respCode = 200;

        url = sboClaim.claim.getAttribute("href");
        System.out.println("url = " + url);

        try {
            huc = (HttpURLConnection) (new URL(url).openConnection());

            huc.setRequestMethod("HEAD");

            huc.connect();

            respCode = huc.getResponseCode();

            Assert.assertEquals(200, respCode);

            if (respCode >= 400) {
                System.out.println(url + " is a broken link");
            } else {
                System.out.println(url + " is a valid link");
            }

        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}



