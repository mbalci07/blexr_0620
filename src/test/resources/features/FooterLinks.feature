@all
Feature: Verify the links in the site’s footer.

  @FooterLinks
  Scenario:Verify that none of the links in the site’s footer is broken
    Given User should be see home page
    Then Verify each link is not broken
