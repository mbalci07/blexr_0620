$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/features/SboClaim.feature");
formatter.feature({
  "name": "The bookie selector function",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "“CLAIM” button link should not be broken with the following options",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@wipBonus"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "User should be on sbo.net site.",
  "keyword": "Given "
});
formatter.match({
  "location": "SboClaimStepDefs.user_should_be_on_sbo_net_site()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User clicks on the Bookie Selector",
  "keyword": "When "
});
formatter.match({
  "location": "SboClaimStepDefs.user_click_on_the_Bookie_Selector()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Select the following following choices Desktop, €50 - €200, Yes",
  "keyword": "And "
});
formatter.match({
  "location": "SboClaimStepDefs.select_the_following_following_choices_Desktop_€_€_Yes(Integer,Integer)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Verify that CLAIM button link is not broken.",
  "keyword": "Then "
});
formatter.match({
  "location": "SboClaimStepDefs.verify_that_CLAIM_button_link_is_not_broken()"
});
formatter.result({
  "error_message": "java.lang.AssertionError: expected:\u003c200\u003e but was:\u003c403\u003e\r\n\tat org.junit.Assert.fail(Assert.java:88)\r\n\tat org.junit.Assert.failNotEquals(Assert.java:834)\r\n\tat org.junit.Assert.assertEquals(Assert.java:645)\r\n\tat org.junit.Assert.assertEquals(Assert.java:631)\r\n\tat com.blexr.stepDefinitions.SboClaimStepDefs.verify_that_CLAIM_button_link_is_not_broken(SboClaimStepDefs.java:68)\r\n\tat ✽.Verify that CLAIM button link is not broken.(file:src/test/resources/features/SboClaim.feature:8)\r\n",
  "status": "failed"
});
formatter.embedding("image/png", "embedded0.png", null);
formatter.after({
  "status": "passed"
});
});