package com.blexr.pages;

import com.blexr.utilities.Driver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;
import java.util.List;

public class RealMoney {
    public RealMoney() {
        PageFactory.initElements(Driver.get(), this);
    }

    @FindBy(xpath = ("//div[@class='grid-container']//a[contains(text(),'Real Money')]"))
    public WebElement realMoneyTab;

    @FindBy(xpath = "(//a[contains(text(),'Table Games')])[2]")
    public WebElement tableGames;

    @FindBy(xpath = "//a[contains(text(),'New Online Slots')]")
    public WebElement newOnlineSlot;

    @FindBy(xpath = "//a[contains(text(),'live casino')]")
    public WebElement liveCasino;

    @FindBy(xpath = "//a[contains(text(),'payment methods')]")
    public WebElement paymentMethods;

    @FindBy(xpath = "//div[@class='right-side']//a[@class='cta-button transparent'][contains(text(),'Our Providers')]")
    public WebElement onlineProviders;


    public List<WebElement> clickLinks() {
        List<WebElement> linkList = new ArrayList<>();
        linkList.add(liveCasino);
        linkList.add(paymentMethods);
        linkList.add(tableGames);
        linkList.add(newOnlineSlot);
        linkList.add(onlineProviders);
        return linkList;
    }

}
